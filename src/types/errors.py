#!/usr/bin/env python
#-'''- coding: utf-8 -'''-

class SecretError():
    def __init__(self, code, error_text):
        self._code = code
        self._error_text = error_text

    @property
    def error_text(self):
        return self._error_text

    @property
    def code(self):
        return self._code

class IsLockedError(SecretError):
    def __init__(self):
        SecretError.__init__("org.freedesktop.Secret.Error.IsLocked", "The object must be unlocked before this action can be carried out.")

class NoSessionError(SecretError):
    def __init__(self):
        SecretError.__init__("org.freedesktop.Secret.Error.NoSession", "The session does not exist.")

class NoSuchObjectError(SecretError):
    def __init__(self):
        SecretError.__init__("org.freedesktop.Secret.Error.NoSuchObject", "No such item or collection exists.")
        
