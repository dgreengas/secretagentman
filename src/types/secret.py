#!/usr/bin/env python
#-'''- coding: utf-8 -'''-

"""
struct Secret {
  ObjectPath session ;

  Array<Byte> parameters ;

  Array<Byte> value ;

  String content_type ;

}
"""


class Secret():
    @property
    def session(self):
        """The session that was used to encode the secret.
        """
        return self._session

    @session.setter
    def session(self, value):
        self._session = value

    @property
    def parameters(self):
        """Algorithm dependent parameters for secret value encoding.
        """
        return self._parameters

    @parameters.setter
    def parameters(self,value):
        self._parameters = value

    @property
    def value(self):
        """Possibly encoded secret value
        """
        return self._value

    @value.setter
    def value(self,value):
        self._value = value

    @property
    def content_type(self):
        """The content type of the secret. For example: 'text/plain; charset=utf8'
        """
        return self._content_type

    @content_type.setter
    def content_type(self,value):
        self._content_type = value