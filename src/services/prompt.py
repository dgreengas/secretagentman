#!/usr/bin/env python
#-'''- coding: utf-8 -'''-


import dbus
import dbus.service
from .constants import Constants

class SecretPrompt(dbus.service.Object):
    def __init__(self,conn, object_path, bus_name=None):
        # export this object to dbus
        dbus.service.Object.__init__(self,conn,object_path)

    @dbus.service.method("org.freedesktop.Secret.Prompt", in_signature='s', out_signature='')
    def Prompt(self, window_id):
        """Perform the prompt.

        Args:
            window_id (string): Platform specific window handle to use for showing the prompt.
        """
        pass

    @dbus.service.method("org.freedesktop.Secret.Prompt", in_signature='', out_signature='')
    def Dismiss(self):
        """Dismiss the Prompt
        """
        pass

    @dbus.service.signal("org.freedesktop.Secret.Prompt", signature='bv')
    def Completed(self, dismissed, result):
        """The prompt and operation completed.

        Args:
            dismissed (boolean): Whether the prompt and operation were dismissed or not.
            result (variant): The possibly empty, operation specific, result.
        """
        pass

