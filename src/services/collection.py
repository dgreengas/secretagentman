#!/usr/bin/env python
#-'''- coding: utf-8 -'''-


import dbus
import dbus.service
import dbus.mainloop.glib
import random
from .constants import Constants

"""
Delete (	OUT ObjectPath prompt);
 
SearchItems (	IN Dict<String,String> attributes,
 	OUT Array<ObjectPath> results);
 
CreateItem (	IN Dict<String,Variant> properties,
 	IN Secret secret,
 	IN Boolean replace,
 	OUT ObjectPath item,
 	OUT ObjectPath prompt);
"""


class SecretCollection(dbus.service.Object):
    def __init__(self, conn, object_path, bus_name=None):
        # export this object to dbus
        dbus.service.Object.__init__(self,conn,object_path)
    
    @dbus.service.method("org.freedesktop.Secret.Collection", in_signature='', out_signature='o')
    def Delete(self):
        """Delete this collection.

        Returns:
            prompt (path): A prompt to delete the collection, or the special value '/' when no prompt is necessary.
        """
        prompt = None
        return prompt
    
    @dbus.service.method("org.freedesktop.Secret.Collection", in_signature='a{ss}', out_signature='ao')
    def SearchItems(self,attributes):
        """Search for items in this collection matching the lookup attributes.
   
        Args
            attributes (dict<string,string>): Attributes to match.

        Returns
            results (array<object>): Items that matched the attributes.
        """
        results=None
        return results

    @dbus.service.method("org.freedesktop.Secret.Collection", in_signature="a{sv}o(oayays)b", out_signature="oo")
    def CreateItem(self, properties, secret, replace):
        """Create an item with the given attributes, secret and label. If replace is set, then it replaces an item already present with the same values for the attributes.

        Args:
            properties (array<string,variant>): The properties for the new item.
            secret (secret): The secret to store in the item, encoded with the included session.
            replace (boolean): Whether to replace an item with the same attributes or not.

        Returns:
            item (path): The item created, or the special value '/' if a prompt is necessary.
            prompt (path): A prompt object, or the special value '/' if no prompt is necessary.
        """

        """Example of properties
        properties = {
               "org.freedesktop.Secret.Item.Label": "MyItem",
               "org.freedesktop.Secret.Item.Attributes": {
                          "Attribute1": "Value1",
                          "Attribute2": "Value2"
                    }
             }
        """
        item = None
        prompt = None
        return item,prompt

    @dbus.service.signal("org.freedesktop.Secret.Collection", signature='o')
    def ItemCreated(self, item):
        """A new item in this collection was created.

        Args:
            item (path): The item that was created.
        """
        pass

    @dbus.service.signal("org.freedesktop.Secret.Collection", signature='o')
    def ItemChanged(self, item):
        """A new item in this collection was changed.

        Args:
            item (path): The item that was changed.
        """
        pass

    @dbus.service.signal("org.freedesktop.Secret.Collection", signature='o')
    def ItemDeleted(self, item):
        """An item in this collection was deleted.

        Args:
            item (path): The item that was deleted.
        """
        pass

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="", out_signature="ao")
    def Items(self):
        """Property Items
        
        Returns:
            items (array(path)): items
        """
        items=None
        return items

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="s", out_signature="s")
    def Label(self, label=None):
        """Property Read/Write Label

        Args:
            label (string): label for this collection

        Returns:
            label (string): The displayable label of this collection.
        """
        if label:
            self._label = label
        else
            return self._label

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="", out_signature="b")
    def Locked(self):
        """Property read locked

        Returns:
            locked (boolean): is the collection locked
        """
        return self._is_locked

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="", out_signature="t")
    def Created(self):
        """Property read Created

        Returns:
            created (unit64): The unix time when the collection was created.
        """
        return self._created
    
    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="", out_signature="t")
    def Modified(self):
        """Property read Modified

        Returns:
            modified (uint64): The unix time when the collection was last modified.
        """
        return self._modified

    