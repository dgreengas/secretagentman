#!/usr/bin/env python
#-'''- coding: utf-8 -'''-


import dbus
import dbus.service
from .constants import Constants

class SecretSession(dbus.service.Object):
    def __init__(self,conn, object_path, bus_name=None):
        # export this object to dbus
        dbus.service.Object.__init__(self,conn,object_path)

    @dbus.service.method("org.freedesktop.Secret.Session", in_signature='', out_signature='')
    def Close(self):
        """close the session
        """
        pass