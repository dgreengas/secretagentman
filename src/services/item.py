#!/usr/bin/env python
#-'''- coding: utf-8 -'''-


import dbus
import dbus.service
from .constants import Constants

"""
Delete (	OUT ObjectPath Prompt);
 
GetSecret (	IN ObjectPath session,
 	OUT Secret secret);
 
SetSecret (	IN Secret secret);

"""

class SecretItem(dbus.service.Object):
    def __init__(self,conn, object_path, bus_name=None):
        # export this object to dbus
        dbus.service.Object.__init__(self,conn,object_path)

    @dbus.service.method("org.freedesktop.Secret.Item", in_signature='', out_signature='o') 
    def Delete(self):
        """Delete this item.

        Returns:
            Prompt (path): A prompt object, or the special value '/' if no prompt is necessary.
        """
        prompt=None
        return prompt

    @dbus.service.method("org.freedesktop.Secret.Item", in_signature='o', out_signature='o')
    def GetSecret(self, session):
        """Retrieve the secret for this item.

        Args:
            session (path): The session to use to encode the secret.

        Returns:
            secret (secret): The secret retrieved.
        """
        secret = None
        return secret

    @dbus.service.method("org.freedesktop.Secret.Item", in_signature='o', out_signature='')
    def SetSecret(self, secret):
        """Set the secret for this item.

        Args:
            secret (secret): The secret to set, encoded for the included session
        """
        pass

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="", out_signature="b")
    def Locked(self):
        """Property locked

        Returns:
            locked (boolean): Whether the item is locked and requires authentication, or not.
        """
        locked=None
        return locked

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="a{ss}", out_signature="a{ss}")
    def Attributes(self, attributes=None):
        """Proerty read/write attributes

        Args:
            attributes (dict(string,string)): The lookup attributes for this item.

        Returns:
            attributes (dict(string,string)): The lookup attributes for this item.
        """
        if attributes:
            self._attributes = attributes
        else:
            return self._attributes

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="s", out_signature="s")
    def Label(self, label=None):
        """Property read/write label

        Args:
            label (string):

        Returns:
            label (string):

        """
        if label:
            self._label = label
        else:
            return self._label

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="", out_signature="t")
    def Created(self):
        """Property read Created

        Returns:
            created (unit64): The unix time when the item was created.
        """
        return self._created
    
    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="", out_signature="t")
    def Modified(self):
        """Property read Modified

        Returns:
            modified (uint64): The unix time when the item was last modified.
        """
        return self._modified    