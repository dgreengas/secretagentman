#!/usr/bin/env python
#-'''- coding: utf-8 -'''-

# DBUS Server Example of use PySide2 with dbus-python library

import dbus
import dbus.service
import dbus.mainloop.glib
import random
from .constants import Constants

class SecretService(dbus.service.Object):
    def __init__(self, conn, object_path, bus_name=None):
        # export this object to dbus
        dbus.service.Object.__init__(self,conn,object_path)



    @dbus.service.method("org.freedesktop.Secret.Service", in_signature='sv', out_signature='vo')
    def OpenSession(self, algorithm, input ):
        """create a new session for a client

        Args:
            algorithm (string): the encryption algothrim
            input (variant): Input arguments for the algorithm.
        Returns:
            output (variant): output of the session algorithm negotiation.
            path (path): The object path of the session, if session was created.

        """
        output = ''
        path = ''
        return output,path


    @dbus.service.method("org.freedesktop.Secret.Service", in_signature='a{sv}s', out_signature='oo')
    def CreateCollection(self, properties, alias):
        """Create a new collection with the specified properties.

        Args:
            properties (dict): Properties for the new collection. This allows setting the new collection's properties upon its creation. All READWRITE properties are useable. Specify the property names in full interface.Property form.
            alias (string): If creating this connection for a well known alias then a string like default. If an collection with this well-known alias already exists, then that collection will be returned instead of creating a new collection. Any readwrite properties provided to this function will be set on the collection.
        Returns:
            collection (path): The new collection object, or '/' if prompting is necessary.
            prompt (path): A prompt object if prompting is necessary, or '/' if no prompt was needed.

        """
        collection = ''
        prompt = ''
        return collection,prompt
    
    @dbus.service.method("org.freedesktop.Secret.Service", in_signature='a{ss}', out_signature='aoao')
    def SearchItems(self, attributes):
        """Find items in any collection.

        Args:
            attributes (dict): Find secrets in any collection
        Returns:
            unlocked (array<path>): Items found.
            locked (array<path>): Items found that require authentication.
        """
        unlocked=None
        locked=None
        return unlocked, locked
    
    @dbus.service.method("org.freedesktop.Secret.Service", in_signature='ao', out_signature='aoo')
    def UnLock(self, objects):
        """Unlock the specified objects.

        Args:
            objects (array<path>): Objects to unlock.
        Returns:
            unlocked (array<path>): Objects that were unlocked without a prompt.
            prompt (path): A prompt object which can be used to unlock the remaining objects, or the special value '/' when no prompt is necessary.
        """
        unlocked=None
        prompt=None
        return unlocked,prompt

    @dbus.service.method("org.freedesktop.Secret.Service", in_signature='ao', out_signature='aoo')
    def Lock(self, objects):
        """lock the specified objects.

        Args:
            objects (array<path>): Objects to   lock.
        Returns:
            locked (array<path>): Objects that were locked without a prompt.
            prompt (path): A prompt object which can be used to lock the remaining objects, or the special value '/' when no prompt is necessary.
        """
        locked=None
        prompt=None
        return locked,prompt

    @dbus.service.method("org.freedesktop.Secret.Service", in_signature='aoo',out_signature='a{o(oayays)}')
    def GetSecrets(self, items, session):
        """Retrieve multiple secrets from different items.

        Args:
            items (array<path>): Items to get secrets for.
            session (path): The session to use to encode the secrets.
            
        Returns:
            secrets (dict<path,secret>): Secrets for the items.
        """
        secrets=None
        return secrets

    @dbus.service.method('org.freedesktop.Secret.Service', in_signature='s', out_signature='o')
    def ReadAlias(self, name):
        """Get the collection with the given alias.

        Args:
            name (string): An alias, such as 'default'.

        Returns:
            collection (path): The collection or the the path '/' if no such collection exists.
        """
        collection=None
        return collection

    @dbus.service.method('org.freedesktop.Secret.Service', in_signature='so', out_signature='')
    def SetAlias(self, name, collection):
        """Setup a collection alias.

        Args:
            name (string): An alias, such as 'default'.
            collection (path): The collection to make the alias point to. To remove an alias use the special value '/'.
        """
        pass

    @dbus.service.signal("org.freedesktop.Secret.Service", signature='o')
    def CollectionCreated(self, collection):
        """A collection was created.

        Args:
            collection (path): Collection that was created
        """
        pass


    @dbus.service.signal("org.freedesktop.Secret.Service", signature='o')
    def CollectionDeleted(self, collection):
        """A collection was deleted.

        Args:
            collection (path): Collection that was deleted
        """
        pass


    @dbus.service.signal("org.freedesktop.Secret.Service", signature='o')
    def CollectionChanged(self, collection):
        """A collection was changed.

        Args:
            collection (path): Collection that was changed
        """
        pass

    @dbus.service.method(interface=dbus.PROPERTIES_IFACE, in_signature="", out_signature="ao")
    def Collections(self):
        """Property collections

        Returns:
            collections (array(path)): The object paths of all collections (ie: keyrings)
        """
        collections = None
        return collections


