#!/usr/bin/env python
#-'''- coding: utf-8 -'''-

import dbus
import dbus.service
import dbus.mainloop.glib
import random

from PySide2.QtCore import *
from PySide2.QtWidgets import QPushButton, QApplication

from services.service import SecretService


if __name__ == "__main__":
    print("holi")
    app = QApplication([])
    # Use qt/glib mainloop integration to get dbus mainloop working
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    
    session_bus = dbus.SessionBus()
    name = dbus.service.BusName("org.freedesktop.Secret", session_bus)
    service = SecretService(session_bus, '/org/freedesktop/secrets')
    app.exec_()
    #r=subprocess.run(args,capture_output=True)
