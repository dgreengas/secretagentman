**secretagentman**
===========
Secretagentman is a python/Qt implementation of Secret Service API. Instead of focusing on how to build a back-end storage mechanism, it intends to leverage your own backend, with LastPass as the first choice.

Core Features
-------------
* Expose [![freedestkop secrets api](https://standards.freedesktop.org/secret-service/index.html)]
* Abstract backend calls versus replying to d-bus service
* Support LastPass backed via lpass cli tool


More Information
----------------
* Relies on d-bus and python documentation at [dbustypes](https://pythonhosted.org/txdbus/dbus_overview.html)


